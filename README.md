# Coding standards tools

Here we list a non-exhaustive set of tools for managing coding-standards and related systems

## C/C++
### External Coding Standards
* [Bloomberg](https://github.com/bloomberg/bde/wiki/Introduction-to-BDE-Coding-Standards)
* [Google](https://google.github.io/styleguide/cppguide.html)
* [LLVM](https://llvm.org/docs/CodingStandards.html)
* [Mozilla](https://www-archive.mozilla.org/hacking/mozilla-style-guide.html)
* [Webkit](https://webkit.org/code-style-guidelines/)
### Internal Coding Standards
* TODO
### Tools
* [clang-tidy](http://clang.llvm.org/extra/clang-tidy/) - static analysis and modernising
  * [clang-tidy and cmake3.6](http://mariobadr.com/using-clang-tidy-with-cmake-36.html)
  * Configs
    * [Google clang-tidy config](config_examples/clang-tidy/6.0/google.yml)
    * [LLVM clang-tidy config](config_examples/clang-tidy/6.0/llvm.yml)
    * [Mozilla clang-tidy config](config_examples/clang-tidy/6.0/mozilla.yml)
    * [Webkit clang-tidy config](config_examples/clang-tidy/6.0/webkit.yml)
* [clang-format](https://clang.llvm.org/docs/ClangFormat.html) - auto formating
* [cppcheck](http://cppcheck.sourceforge.net/)
  * [add cppcheck to cmake](https://arcanis.me/en/2015/10/17/cppcheck-and-clang-format)
* [bde-verify](https://github.com/bloomberg/bde_verify) - Bloomberg Basic Development Environment conformance tool (build on top of clang)

## Javascript/ECMA
### External Coding Standards
* [BBC](http://www.bbc.co.uk/guidelines/futuremedia/technical/javascript.shtml)
### Internal Coding Standards
* [TODO]()
### Tools
* [TODO]()

## Perl
### External Coding Standards
* [perlstyle](http://perldoc.perl.org/perlstyle.html)
### Internal Coding Standards
* [TODO:]()
### Tools
* [Perl::Analyse::Static](https://metacpan.org/pod/release/GGOLDBACH/Perl-Analysis-Static-0.004-TRIAL/lib/Perl/Analysis/Static.pm)
* [Perl::Critic](http://search.cpan.org/dist/Perl-Critic/) - [config examples](config_examples/perl/perl_critic/)

## Python
### External Coding Standards
* [PEP 8](https://www.python.org/dev/peps/pep-0008/) - most common coding style
* [PEP 257](https://www.python.org/dev/peps/pep-0257/) - doc string conventions
### Internal Coding Standards
* [TODO:]()
### Tools
* [autopep8](https://pypi.org/project/autopep8/) - auto-fixes pep8 breaking code
* [pycodestyle](https://github.com/PyCQA/pycodestyle) - used to be called pep8. Runs pepe8 checks 
  * [using pycodestyle check in unittests](https://pycodestyle.readthedocs.io/en/latest/advanced.html#automated-tests)
* [flake8](https://readthedocs.org/projects/flake8/) - wraps PyFlakes, pycodestyle, etc


